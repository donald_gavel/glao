# -*- coding: utf-8 -*-
""" GLAO structure function
  To support white paper.
"""
import os
import sys
import numpy as np
from common_units import *
import re
import subprocess

def test():
    for pfile in ['0','2p5arcmin','5arcmin']:
        filename = 'TMT_PSF_%s.txt'%pfile
        f = open(filename,'r')
        s = f.read()
        f.close()
        
        # separate lines by \r
        # remove all nulls \00
        # split lines on tabs \t
        lines = (s.split('\r'))
        rows = []
        for line in lines[21:-1]:
            line = line.replace('\00','').strip()
            line = re.split('\s+',line)
            row = map(float,line)
            rows.append(row)
        
        psf = np.array(rows)
        psf = psf/psf.sum()
        focal_length = 450.
        dx = 1.457*microns/focal_length/arcsec
        
        ENA = dx**2 / (psf**2).sum()
        ENA_R = np.sqrt(ENA/np.pi)
        print '%s: ENA = %r arcsec**2, ENA_R = %r arcsec'%(pfile,ENA,ENA_R)

def ena(filename):
    f = open(filename,'r')
    s = f.read()
    f.close()
    
    # separate lines by \r
    # remove all nulls \00
    # split lines on tabs \t
    lines = (s.split('\r'))
    rows = []
    for line in lines[21:-1]:
        line = line.replace('\00','').strip()
        line = re.split('\s+',line)
        row = map(float,line)
        rows.append(row)
    
    psf = np.array(rows)
    psf = psf/psf.sum()
    focal_length = 450.
    dx = 1.457*microns/focal_length/arcsec
    
    ENA = dx**2 / (psf**2).sum()
    ENA_R = np.sqrt(ENA/np.pi)
    print '%s: ENA = %r arcsec**2, ENA_R = %r arcsec'%(filename,ENA,ENA_R)

def ena2(filename):
    if not os.path.isfile(filename):
        raise Exception,'file %s does not exist'%filename
    r = subprocess.call(['dos2unix',filename])
    f = open(filename,'r')
    lines = f.read()
    f.close()
    lines = lines.split('\n')
    
    # build up the header
    hdr = {}
    hdr['explanation'] = lines[0]
    hdr['filename'] = lines[2].split(' : ')[1].strip()
    hdr['date'] = lines[4].split(' : ')[1].strip()
    # line 9 has dx
    line = lines[9].split()
    hdr['dx'] = float(line[3])
    hdr['dx_units'] = line[4][:-1].decode('utf8')
    
    # read the data. it starts at line 21 and ends at the second to last line
    rows = []
    for line in lines[21:-2]:
        row = map(float,line.split())
        rows.append(row)
    a = np.array(rows)
    return hdr,a
