# zernike.py
# http://code.google.com/p/pyoptools/source/browse/pyoptools/wavefront/zernike/zernike.py

"""Module defining the Zernike polynomials
"""

import types
from scipy import arange
from scipy.misc import factorial, comb as binomial
from scipy.special import jn as besselj
import numpy as np
import img
from numpy import mgrid, sqrt, arccos, where, zeros,transpose, pi, cos, sin, ones, array,\
                where
from numpy.ma import masked_array, make_mask_none
from oprint import pprint
from info_array import InfoArray

class Zernike(object):
    def __init__(self,n,m):
        assert n>=0 and m>=0
        assert is_even(n-m)
        self.n,self.m = n,m
        self.coefs = rnm_coefs(n,m)
        self.poly()
    
    def pprint(self):
        """pretty-print the Zernike object
        """
        pprint(self)
        
    def poly(self,format='text'):
        """print the polynomial representation
        in text or LaTeX format
        """
        assert format in ['text','TeX']
        if hasattr(self,'rep_txt'):
            if format == 'text': return self.rep_txt
            elif format == 'TeX': return self.rep_TeX
        
        n,m = self.n,self.m
        cs = sorted(self.coefs,reverse=True)
        tpoly = 'R%d%d(r) = '%(n,m)
        lpoly = 'R_{%d}^{%d}(r) = '%(n,m)
        first = True
        for p,c in cs:
            if not first and c>0:
                tpoly += ' + '
                lpoly += '+'
            if not first and c<0:
                tpoly += ' - '
                lpoly += '-'
            if (c!=1 or n==0 or p==0):
                tpoly += '%d'%abs(c)
                lpoly += '%d'%abs(c)
            first = False
            if (p>0):
                tpoly += 'r'
                lpoly += 'r'
            if (p>1):
                tpoly += '^%d'%p
                lpoly += '^{%d}'%p
        self.rep_txt = tpoly
        self.rep_TeX = '$'+lpoly+'$'
        if format == 'TeX':
            return lpoly
        else:
            return tpoly
        
    def func(self,N=128,r1=0.,r2=1.):
        """produce the Zernike 1D function sampled at N data points
        over a sample interval from r1 to r2
        """
        x = np.linspace(r1,r2,N)
        Z = 0
        for p,c in self.coefs:
            Z += c*x**p
        Z = InfoArray(Z,name='Zernike %d,%d'%(self.n,self.m),
                      n=self.n,m=self.m,x0=x[0],dx=x[1]-x[0])
        return x,Z
    
    def func2D(self,N=128,rmax=1.,calc='both'):
        """produce the Zernike 2D funcion sampled at NxN data points
        
        Keyward args
            calc = 'both' means if m is not 0, return both Znm*cos(m theta) and Znm*sin(m theta) in a tuple
                   'cos' or 'sin' returns only the one
                    If m=0 then Znm is returned
        """
        assert calc in ['both','cos','sin']
        n,m = self.n,self.m
        if m == 0: calc = 'cos'
        if calc == 'both': calc = ['cos','sin']
        else: calc = [calc]
        
        dr = 2*rmax/float(N)
        x = np.arange(-rmax,rmax,dr)
        x,y = np.meshgrid(x,x)
        r = np.sqrt(x**2+y**2)
        th = np.arctan2(y,x)
        Znm = 0
        for p,c in self.coefs:
            Znm += c*r**p
        
        Zr = []
        for cs in calc:
            if m == 0:
                Z = Znm
            elif cs == 'cos':
                Z = Znm*np.cos(m*th)
            elif cs == 'sin':
                Z = Znm*np.sin(m*th)
            
            name = r'$Z_{%d}^{%d}(\rho)$'%(self.n,self.m)
            if m != 0:
                name += r'$%s %d \theta$'%(cs,m)
            Z = InfoArray(Z,name = name, parity=cs,
                  n=self.n,m=self.m,x0y0=(x[0,0],y[0,0]),dx=dr)
            ap = img.circle((N,N),r=1./dr)
            Z.ap = ap
            Zr.append(Z)
        
        if len(Zr) == 1:
            Zr = Zr[0]
        return Zr
    
def is_odd(n):
    assert isinstance(n,int)
    return (n%2)

def is_even(n):
    assert isinstance(n,int)
    return 1 - (n%2)

def ztable(kmax,noll=True):
    """generate a table of valid zernike modes in the (n,m) designation.
    
    This is a rewrite of the zernike.pro routine
    """
    tab = []
    n = 0
    m = 0
    s = 0
    if not noll:
        kmax += 1
    for k in range(kmax):
        if noll:
            if m != 0:
                s = is_odd(k+1)
            if s:
                tab.append((n,-m))
            else:
                tab.append((n,m))
        else:
            tab.append((n,m,s))
        
        if noll:
            if n != 0 and m != 0:
                if tab[-1][1] == -tab[-2][1]:
                    m = m+2
            else:
                m = m+2
            if m > n:
                n = n+1
                m = is_odd(n)
        else:
            if m != 0 and s == 0:
                s = 1
            else:
                m = m+2
                s = 0
            if m > n:
                n = n+1
                if not is_odd(n):
                    m = 0
                else:
                    m = 1
                s = 0
    return tab

def zernike2(x,y,r,n,m,c=None,l=None,table=None,noll=True):
    """Create the Noll zernike function at x and y. If x and y are integers,
    they are interpreted as the size of an array over which to compute the
    zernike. Units of x,y, r, and c are pixels. c must be a 2-tuple (x and y center)
    
    This is a rewrite of the zernike.pro routine
    """
    if is_odd(n) != is_odd(m) or np.abs(m) > n:
        raise ValueError,'<_zernike> invalid n,m pair: '+str((n,m))
    if noll:
        if m < 0:
            m = np.abs(m)
            l = 1
        else:
            l = 0
    if l is not None:
        assert l in [0,1]
    if isinstance(x,int):
        nx = x
        ny = y
        if c is None:
            c = [nx/2.,ny/2.]
        x = np.arange(nx) - c[0]
        y = np.arange(ny) - c[1]
        x,y = np.meshgrid(x,y)
    u = np.sqrt( x**2 + y**2 )/r
    th = np.arctan2(y,x)
    p = 0*u
    for s in range((n-m)/2+1):
        p = p + (-1)**s * (np.math.factorial(n-s) / np.math.factorial(s) ) / \
                          (np.math.factorial((n+m)/2-s) * np.math.factorial((n-m)/2-s)) \
                        * u**(n-2*s)
    p *= np.sqrt(n+1)
    if l is None:
        ret = (p*np.cos(m*th),p*np.sin(m*th))
    elif l == 0:
        ret = p*np.cos(m*th)
    elif l == 1:
        ret = p*np.sin(m*th)
    if m != 0:
        ret *= np.sqrt(2.)
    return ret

def set(n,nz,r=None):
    """return a set of zernikes up to number nz
    (n,n) is the size of the returned screen
    """
    nx = n
    table = ztable(nz,noll=True)
    if r is None:
        r = n/2
    zset = []
    for n,m in table:
        zset.append(zernike2(nx,nx,r,n,m,noll=True))
    return zset
    
def series(coef,n,r,c=None):
    """generate a shape that is the sum of zernikes according to coefficients
    """
    nx,ny = n,n
    p = np.zeros((nx,ny))
    nz = len(coef)
    tab = ztable(nz,noll=True)
    for cc,nm in zip(coef,tab):
        n,m = nm
        p += cc*zernike2(nx,ny,r,n,m,c=c,noll=True)
    return p

def fit(p,r,nz,c=None):
    """find the coefficients that best fit a seriez of zernikes to a given shape
    """
    nx,ny = p.shape
    table = ztable(nz,noll=True)
    coef = np.zeros((nz,))
    fit = np.zeros((nx,ny))
    ap = img.circle((nx,ny),r = r)
    for k in range(nz):
        n,m = table[k]
        z = zernike2(nx,ny,r,n,m,c=c)
        coef[k] = (z*p*ap).sum() / (z*z*ap).sum()
        fit += coef[k]*z
    return (coef,fit,ap)

def fourier(kx,ky,r,n,m):
    """generate the Fourier transform of the n,m zernike
    kx and ky are spatial frequency, in units of 1/radius
    r is the radius in pixels
    if kx and ky are integers, they represent the size of a 2d array
    """
    i = 1j
    fuzz = 1.e-8
    if isinstance(kx,int):
        nx = kx
        ny = ky
        kx = np.arange(-nx/2,nx/2)
        ky = np.arange(-ny/2,ny/2)
        kx,ky = np.meshgrid(kx,ky)
    k = np.sqrt(kx**2+ky**2)
    k[nx/2,ny/2] = fuzz
    phi = np.arctan2(ky,kx)
    q = np.sqrt(n+1)*(besselj(n+1,2*np.pi*k)/(np.pi*k))*(-1)**((n-m)/2)*i^m
    if m > 0:
        q *= np.sqrt(2.)*np.cos(m*phi)
    else:
        q *= np.sqrt(2.)*np.sin(m*phi)
    return q
    
def graph(nmax=9,npix=100):
    """Draw a Zernike "Pascal triangle" graphic
    nmax = 9 # go up to this order
    npix = 100 # number of pixels across a circle in the graph
    """
    nbuf = npix/5
    nn = (nmax+1)*(npix+nbuf)
    g = np.zeros((nn,nn))
    dx = (npix + nbuf)/2
    dy = npix+nbuf
    kmax = ((nmax+1)*(nmax+2))/2
    ztab = ztable(kmax,noll=True)
    ap = img.circle((npix,npix),c=(npix/2,npix/2),r=npix/2)
    for k in range(kmax):
        n,m = ztab[k]
        x = m*dx + nn/2
        y = nn - (n*dy + npix/2)
        z = ap*zernike2(npix,npix,npix/2,n,m,noll=True)
        g[y-npix/2:y+npix/2,x-npix/2:x+npix/2] = z
    return g
    
def polar_array(Rmax=1.,DS=0.1, pr=1., c = [0.,0.]):
    """
    Function that generates 2 square matrices one with the rho coordinate,
    and the other with the theta coordinate, to be able to calculate 
    functions using polar coordinates.
    
    It is similar to the mgrid function.

    **Arguments**
    
    ==== ===================================================
    Rmax Limit the pupil area -Rmax<=X<=Rmax -Rmax<=Y<=Rmax
    DS   Step between pixels
    pr   Pupil radius. Used to normalize the pupil.
    ==== ===================================================
        
    ..  TODO:: This function should be moved to a auxiliary functions module
    """
    
    X,Y= mgrid[-Rmax-c[0]:Rmax-c[0]+DS:DS,-Rmax-c[1]:Rmax-c[1]+DS:DS]/pr
    r = sqrt(X**2+Y**2)
    th= arccos(transpose(X*1./r))
    th= where(th<2.*pi,th,0)
    th= where(X<0,2.*pi-th,th)
    return r,th

def rnm_coefs(n,m):
    """ return a list of (power,coef) for the Zernike polynomial Z_n^m(r)
    """
    s = []
    for k in range(0,(n-m)/2+1):
        c = (-1)**k *factorial(n-k) / (factorial(k)*factorial((n+m)/2-k)*factorial((n-m)/2-k))
        s.append((n-2*k,c))
    return s

def rnm(n,m,rho,limit=True):
    """
    Return an array with the zernike Rnm polynomial calculated at rho points.
    
    
    **ARGUMENTS:**
    
        === ==========================================
        n    n order of the Zernike polynomial
        m    m order of the Zernike polynomial
        rho  Matrix containing the radial coordinates. 
        === ==========================================       
    
    .. note:: For rho>1 the returned value is 0
    
    .. note:: Values for rho<0 are silently returned as rho=0
    
    """
    
    if(type(n) is not int):
        raise Exception, "n must be integer"
    if(type(m) is not int):
        raise Exception, "m must be integer"
    if (n-m)%2!=0:
        raise Exception, "n-m must be even"
    if abs(m)>n:
        raise Exception, "The following must be true |m|<=n"
    if (limit):
        mask=where(rho<=1,False,True)
    else:
        mask = make_mask_none(rho.shape)
        
    if(n==0 and m==0):
        return  masked_array(data=ones(rho.shape), mask=mask)
    if (limit):
        rho=where(rho<0,0,rho)
    Rnm=zeros(rho.shape)
    S=(n-abs(m))/2
    for s in range (0,S+1):
        CR=pow(-1,s)*factorial(n-s)/ \
            (factorial(s)*factorial(-s+(n+abs(m))/2)* \
            factorial(-s+(n-abs(m))/2))
        p=CR*pow(rho,n-2*s)
        Rnm=Rnm+p
    return masked_array(data=Rnm, mask=mask)
    
def zernike(n,m,rho,theta,limit=True):
    """
    Returns the an array with the Zernike polynomial evaluated in the rho and 
    theta.
    
    **ARGUMENTS:** 
    
    ===== ==========================================     
    n     n order of the Zernike polynomial
    m     m order of the Zernike polynomial
    rho   Matrix containing the radial coordinates. 
    theta Matrix containing the angular coordinates.
    ===== ==========================================
 
    .. note:: For rho>1 the returned value is 0
    
    .. note:: Values for rho<0 are silently returned as rho=0
    """
    
    
    Rnm=rnm(n,m,rho,limit=limit)
    
    NC=sqrt(2*(n+1))
    S=(n-abs(m))/2
    
    if m>0:
        Zmn=NC*Rnm*cos(m*theta)
    #las funciones cos() y sin() de scipy tienen problemas cuando la grilla
    # tiene dimension cero
    
    elif m<0:
        Zmn=NC*Rnm*sin(m*theta)
    else:
        Zmn=sqrt(0.5)*NC*Rnm
    return Zmn
    
def zernike2taylor(n,m):
    """
    Returns the 2D taylor polynomial, that represents the given zernike
    polynomial
    
    **ARGUMENTS**
    
        n,m     n and m orders of the Zernike polynomials
    
    **RETURN VALUE**
        poly2d instance containing the polynomial
    """
    TC=zeros((n+1,n+1))
    mm=(n-m)/2
    am=abs(m)
    if m>0:
        B=mm
        if n%2==0:
            p=1
            q=(m/2)-1
        else:
            p=1
            q=(m-1)/2
    else:
        B=n-mm
        if n%2==0:
            p=0
            q=-(m/2)
        else:
            p=0
            q=-(m+1)/2
    for i in range(q+1):
        for j in range(B+1):
            for k in range(B-j+1):
                c=pow(-1,i+j)*binomial(am,2*i+p)*binomial(B-j,k)*int(factorial(n-j))/(int(factorial(j))*int(factorial(mm-j))*int(factorial(n-mm-j)))
                x_pow=2*(i+k)+p
                y_pow=n-2*(i+j+k)-p
                TC[y_pow,x_pow]=TC[y_pow,x_pow]+c
            
    n=TC.shape[0]-1
    cohef=[0.]*ord2i(n)
    for i in range(ord2i(n)):
        px,py=i2pxpy(i)
        cohef[i]=TC[px,py]
    return poly2d(cohef)

def i2nm(i):
    """
    Return the n and m orders of the i'th zernike polynomial
       
    ========= == == == == == == == == == == == == == == == ===
    i          0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 ...
    n-order    0  1  1  2  2  2  3  3  3  3  4  4  4  4  4 ...
    m-order    0 -1  1 -2  0  2 -3 -1  1  3 -4 -2  0  2  4 ...
    ========= == == == == == == == == == == == == == == == ===
    """
    # Calculate the polynomial order
    #Order      0   1   2   3   4
    #initindex  0   1   3   6   10
    ia=array(i)
    n=(1+(sqrt(8*(ia)+1)-3)/2).astype(int)
    ni=n*(n+1)/2
    m=-n+2*(i-ni)
    return n, m
    
class ZernikeXY(object):
    '''
    Class used to evaluate the zernike polinomial with coheficients 
    given by cohef, in Cartesian coordinates.
        
        
    This class uses an internal Taylor representation of the polynomial
    for all the calculations. The internal taylor representation, is 
    generater from the cohef argument given in the constructor.
    If cohef is changed, the internal taylor representation is build
    again.
    
    **ARGUMENTS**
    
    cohef -- List containing the Zernike polynomial coheficients.
    
    The coheficients given in cohef are enumerated as follows
    
    ========= == == == == == == == == == == == == == == == ===
    index      0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 ...
    n-order    0  1  1  2  2  2  3  3  3  3  4  4  4  4  4 ...
    m-order    0 -1  1 -2  0  2 -3 -1  1  3 -4 -2  0  2  4 ...
    ========= == == == == == == == == == == == == == == == ===
    '''
    
    def __init__(self,cohef=[0]):
            
        self.cohef=cohef
    
    
    def __set_cohef__(self,cohef):
        
        # Save the coheficient list
        self.__cohef__=cohef
        
        #Generate the taylor representation of the zernike polinomial
        
        p=poly2d([0])
        
        for i,c in enumerate(cohef):
            n,m=i2nm(i)
            p=p+c*zernike2taylor(n,m)
        
        self.poly=p

    def __get_cohef__(self):
        return self.__cohef__
    cohef = property(__get_cohef__, __set_cohef__, None, "Coefficient list of the zernike polynomial")

    def eval(self):
        """Not impremented yet"""
        pass
        
    def evalm(self,x,y,mask=True):
        '''
        Evaluate the zernike polynomial 
        
        Method used to evaluate the zernike polynomial at the coordinates 
        given by the 2D arrays x,y
        
        **ARGUMENTS**
        
            ==== ==============================================================
            x    2D array containing the X coordinates where the Zernike 
                 polynomial is to be evaluated.
            y    2D array containing the Y coordinates where the Zernike 
                 polynomial is to be evaluated.
            mask Flag indicating if the evaluated values are to be masked. 
                 If mask is True, the polynomial will only be evaluated at 
                 the pupil sqrt(x**2+y**2)<1. , and a masked array is returned.
            ==== ==============================================================
        
        '''
        
        if mask:
            r=x**2+y**2
            m=where(r<1,False,True)
            retval=masked_array(self.poly.meval(x,y),m)
        else:
            retval=self.poly.meval(x,y)
        return retval
            
    def gpu_eval(self,x,y):
        retval=self.poly.gpu_eval(x,y)
        return retval

