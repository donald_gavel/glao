# calculations for the structure functions of GLAO

import numpy as np
import matplotlib.pylab as plt

try:
    if closem:
        for fig in figs:
            plt.close(fig)
except:
    pass

figs = []
figs.append( plt.figure() )
u = np.arange(-2,2,.01)
w = 1.0
v = (1+w*w)*np.abs(u)**(5./3.) + 2*w - w*np.abs(u+1)**(5./3.) - w*np.abs(u-1)**(5./3.)
v0 = np.abs(u)**(5./3.)

plt.plot(u,v,'k',label='1.0')
plt.plot(u,v0,'k--',label='0')

colors = ['r','g','b','c','m','y']
ws = [.2,.3,.5,.6,.707,.9]
for w,color in zip(ws,colors):
    v = (1+w*w)*np.abs(u)**(5./3.) + 2*w - w*np.abs(u+1)**(5./3.) - w*np.abs(u-1)**(5./3.)
    plt.plot(u,v,color,label=str(w))

ax = plt.gca()
ax.set_xlim([.01,2])
ax.set_ylim([.01,3])
#ax.set_xscale('log')
#ax.set_yscale('log')
plt.grid('on',which='both')
plt.legend(loc='upper left')
plt.title('structure function')
ax.set_xlabel(r'normalized separation, $\mu/\gamma$')
ax.set_ylabel(r'corrected structure function, ${\cal D}(\mu)$')

# optimization
mu = np.arange(.01,2,.01)
w = .5*(np.abs(1+mu)/mu)**(5./3.) + .5*(np.abs(1-mu)/mu)**(5./3.) - 1./mu**(5./3.)
figs.append(plt.figure())
plt.plot(mu,w)
ax = plt.gca()
ax.grid('on')
plt.title('optimum weight')
ax.set_xlabel(r'normalized separation, $\mu/\gamma$')
