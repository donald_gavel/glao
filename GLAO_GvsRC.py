# Supporting code for GLAO_GvsRC.ipynb

import numpy as np
import matplotlib.ticker as ticker
from common_units import arcsec,cm
arcmin = 60.*arcsec

# we'll tie the altitude to the thickness and vary the thickness,
# keeping the lower limit on the ground

sigma = np.linspace(0.1,500.,200) # thickness
hGL = sigma/2 # mean height

r0 = 20.*cm
r0pp = 1.255*r0

plt.figure()

zcas = [50,70,100,130]
lins = ['-','--']
tels = ['G','RC']
cols = ['k','r','g','b']

for zca,col in zip(zcas,cols):
    lab = r'$z_c$ = $\pm$%d m'%zca
    for zc,lin,tel in zip([zca,-zca],lins,tels):
        h1 = hGL - sigma/2 - zc
        h2 = hGL + sigma/2 - zc
        zGL = ((1/sigma)*(3./8.)*(np.sign(h2)*np.abs(h2)**(8./3.) - np.sign(h1)*np.abs(h1)**(8./3.)))**(3./5.)
        thetaGL = 0.314*r0pp/zGL
        if tel == 'G':
            plt.plot(sigma,thetaGL/arcmin,col+lin,label = lab)
        else:
            plt.plot(sigma,thetaGL/arcmin,col+lin)

zca,col = (0,'k')
lab = r'$z_c$ = %d m'%zca
zc,lin,tel = (0,':','DM conjugate to ground (0km)')
h1 = hGL - sigma/2 - zc
h2 = hGL + sigma/2 - zc
zGL = ((1/sigma)*(3./8.)*(np.sign(h2)*np.abs(h2)**(8./3.) - np.sign(h1)*np.abs(h1)**(8./3.)))**(3./5.)
thetaGL = 0.314*r0pp/zGL
plt.plot(sigma,thetaGL/arcmin,col+lin,label = lab)

plt.yscale('log')
plt.legend(loc=1,title='DM Conjugate height')
#plt.xscale('log')
ax = plt.gca()
plt.yticks([1,2,5,10,20])
plt.ylim([0.7,20.])
ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

plt.grid('on')
plt.xlabel(r'Height of ground layer (meters)'+'\n 68.6% of the turbulence must be below this')
plt.ylabel(r'GLAO Isoplanatic angle, $\Theta_{GL}$ (arcmin)')
plt.xlim(0,max(sigma))

plt.text(70,12,'Gregorian (solid)')
plt.text(200,2.,'RC (dash)')

plt.plot([0,max(sigma)],[20,20],'r:')
plt.text(0.5*max(sigma),16,'20 arcmin')
plt.plot([0,max(sigma)],[5,5],'r:')
plt.text(0.55*max(sigma),4.,'5 arcmin')

plt.title(r'$C_n^2$ evenly distributed from ground to height')

