\documentclass[11pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper, margin=1.1in}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage[superscript]{cite}
\usepackage{url}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[font={footnotesize}]{caption}
\captionsetup{margin=10pt}

%SetFonts

%SetFonts


\title{DEIMOS Sensitivity Improvement with GLAO}
\author{Donald Gavel}
\date{6/5/2017}							% Activate to display a given date or no date

\begin{document}
\maketitle
\section{Introduction}

%A bunch of references King\cite{King1983}, Gavel\cite{Gavel2017}, web\cite{DEIMOS}, Chun\cite{Chun2009}, Nelson\cite{Nelson1985}
We investigated whether ground-layer adaptive optics (GLAO) can benefit spectroscopy with the DEIMOS spectrograph on the Keck telescope. DEIMOS is a wide field mult-object spectrograph designed for long exposure extra-Galactic work, particularly observation of distant, early, galaxies. GLAO would reduce the point spread function (PSF) size, which would allow the use of ``good seeing" spectrograph slits ($\sim$1/3 arcsec slit width). This increases the spatial and spectral resolution while reducing the sky background, resulting in an overall increase in instrument sensitivity.

\section{Sensitivity metric}

Sensitivity can be defined in terms of speed, specifically the exposure time needed to achieve a given signal-to-noise ratio (S/N) on a given science traget. The sensitivity metric Equivalent Noise Area (ENA) derived by King$^1$ provides a way to directly compare AO to non-AO performance. The equivalent noise area is defined
$$ ENA = \frac{1}{ \iint PSF(\theta)^2 d^2\theta} $$
assuming the PSF is unit-normalized as $\iint PSF(\theta) d^2\theta = 1$.
The ENA is inversely proportional to the exposure time needed to achieve a given signal-to-noise on a point source in background-limited observations, however it is a reasonable approximation in the photon-limited case as well.

In free-atmosphere seeing, the ENA is$^2$
$$ ENA_{\rm seeing} = \frac{2^{16/5}}{\pi}\left(\frac{\lambda}{r_0}\right)^2 $$
where $r_0$ is the Fried seeing parameter. $r_0$ characterizes the integrated turbulence through the atmosphere.

GLAO will concentrate the PSF light, resulting in larger integrated values of the squared PSF and hence smaller ENA. The sensitivity improvement, or exposure time speedup, is therefore the ratio of the seeing ENA to the expected ENA due to GLAO:
$$ Speedup = \frac{ENA_{\rm seeing}}{ENA} $$

\section{DEIMOS field, pixel scale, and baseline sensitivity}

The DEIMOS science field is 16.7 arcmin $\times$ 5 arcmin, which is much larger that that served by traditional AO. GLAO may increase the sensitivity significantly over a large portion of this field, however, this is not guaranteed in all seeing conditions. If the GLAO system is designed to have a smaller field (i.e. tighter constellation of guidestars), then the observer would need to select slit widths according to position on the field. This is certainly feasible with DEIMOS since slit masks are machined prior to each observation according to observer's specifications.

The DEIMOS pixel scale is 0.1185 arcsec per pixel for both spectrograph and imager$^3$. This would be sufficient to Nyquist-sample a PSF that has been sharpened by GLAO up to a speedup factor of 3.  Speedup factor is proportional to the square of the full-width-half-max improvement, so a speedup factor of 3 corresponds to a full-width-half-max improvement of 1.7, or from open seeing of 0.5 arcsec to GLAO-corrected 0.29 arcsec.

The baseline sensitivity of DEIMOS is documented on the Keck instrument website$^3$:

\vspace{6pt}
\noindent{\it Predicted S/N on faint stellar objects in one hour (low resolution)}$^*$:

5 for V = 24.0;
12 for V = 23.0;
21 for V = 22.0;

\hangindent=24pt
$^*$S/N per pixel for 3600 s integration, 0.75 arcsec slit, on point source with 0.7 arcsec seeing at 5500\r{A} and 0.65\r{A}/px (600 l/mm grating). V is magnitude of light going through slit. Assumed V sky brightness is 21.25 mag/arcsec$^2$.

\vspace{6pt}
\noindent{\it Achieved S/N on faint galaxies in one hour (high resolution)}$^{**}$:

    0.3-0.6 for R = 24.0;
    0.6-1.2 for R = 23.0; 
    1.2-2.4 for R = 22.0; 
    2.2-4.6 for R = 21.0; 

\hangindent=24pt
$^{**}$Achieved S/N per pixel for 3600 s integration through 1.0 arcsec slit near 7000\r{A} with 1200G grating (dispersion of 0.32\r{A}) for typical faint galaxies under average observing conditions. R is total magnitude of target, not accounting for slit losses.

\vspace{6pt}
We assume these baseline sensitivities and report on the exposure time speedup {\it factor} provided by GLAO. Since S/N (signal-to-noise ratio) is proportional to exposure time squared, the S/N improvement factor is the square root of the time speedup factor.

\section{Seeing profile}
The effectiveness of GLAO depends dramatically on the distribution of atmospheric turbulence over alititude. We'll assume the $C_n^2$ profiles of the 2006-2008 survey by Chun$^4$. These are catagorized into good (25 percentile) median (50 percentile) and poor (75 percentile) seeing.  The ground layer portion ($z<$ 600m) is finely sampled while the remainder (\textgreater 600m) is lumped into ``free seeing" (FS). The free seeing statistics were found to be uncorrelated to the ground layer seeing. However, for our analysis, we'll join the corresponding profiles together on each percentile. Figure 2 shows the profiles we used.

\section{GLAO Field of View}
The GLAO Field of View (FoV) is given by the solid angle of atmosphere that the system will average over in order to determine the wavefront correction to be applied. This of course sets the diameter of the constellation needed for guidestars, and essentially this is a design choice. Field average performance of GLAO will be better if the FoV is made narrower and will degrade as the FoV is made wider due to the atmospheric layers at lower and lower altitude being less and less correlated at the wider angles.

We calculate the speedup factor as a function of the chosen Field of View, i.e. solid angle of wavefront averaging. It is important not to confuse this with a variation of ENA over the field. The ENA will be roughly constant over the chosen FoV once the FoV, and accompanying guidestar constellation, is fixed. This more or less constant performance over the field will then drop as the FoV itself, i.e. the wavefront averaging field, is enlarged.

\section{GLAO implementation: importance of the DM conjugate}

Although it is outside the scope of this study to propose designs for GLAO, there is one issue that needs to be highlighted as very important, the conjugate altitude of the deformable mirror. For DEIMOS, AO corrections are required to be effective over a very wide field. To achieve a wide field of common GLAO correction, the conjugate altitude of the deformable mirror must be close to the main altitude of the turbulence.

The typical Maunakea atmosphere has a large portion (about half) of its turbulence concentrated in the first 70-100 meters above the ground.$^4$ However, the
Keck telescope's secondary mirror is optically conjugate to below the ground, at -126 meters$^5$. This will have a significant deleterious impact on the potential for an adaptive secondary mirror helping DEIMOS. We compared the two cases: one where the AO correction is conjugate to the ground (0m) and one where it is conjugate to the secondary conjugate of -126m.

Given the unique nature of the Maunakea $C_n^2$ profile, GLAO has the potential to significantly improve the sensitivity of DEIMOS over its entire 16.8 arcminute $\times$ 5 arcminute field, but this is only possible if the DM is conjugate to the ground. If the adaptive secondary is used, then this field is limited to about 8 arcminute diameter. It is strongly recommended that, to take full advantage of DEIMOS and the Maunakea seeing, one should construct a separate AO relay in front of DEIMOS with the deformable mirror located at a ground conjugate.

\section{PSFs}
Point spread functions were calculated using the analytic technique for computing the average long exposure PSF described in Gavel$^2$. The full set of PSFs generated in support of this report is available in a database.



\bibliographystyle{mybst}
\bibliography{KeckGLAO}


%====================
\noindent
\begin{figure}
\centering
\includegraphics[height=3.4in]{KeckGLAO_Fig1.png}
\caption{{\bf GLAO speedup factor} as a function of Field of View (FoV) of the GLAO system. FoV is that field over which wavefronts are averaged to form the wavefront correction. The red lines indicate the expected ENA speedup for spectroscopy at $\lambda$ = 0.5 $\mu m$ assuming the secondary mirror is used as the deformable mirror. The blue lines are the hypothetical curves for the case of a deformable mirror located at the ground (0m) conjugate. For nominal 50 percentile seeing, the adaptive secondary will provide at least 20\% (factor of 1.2) speedup out to a radius of 4 arcminute. So this would cover a roughly 8' $\times$ 5' portion of the DEIMOS 16.7' $\times$ 5' available field. On the other hand a DM at the 0m conjugate would be able to cover the whole DEIMOS field with at least 70\% (factor of 1.7) speedup. Clearly having the DM conjugate closer to the strong turbulence layers near the ground makes a big difference in the correction FoV.}

\includegraphics[height=3.4in]{KeckGLAO_Fig2.png}
\caption{{\bf Seeing profiles} from the Chun survey summary (Reference 4 Table 6). The free atmosphere seeing is 2.46e-13 (25 percentile), 1.57e-13 (50 percentile), and 9.37e-14 (75 percentile) and placed at $z=$ 5km for this study.}
\end{figure}
%====================

\end{document}
