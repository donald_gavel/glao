GLAO for ELTs
=============

This repository contains code and data for analyzing **Ground Layer Adaptive Optics (GLAO)** on **Extremely Large Telescopes (ELTs)**.

:math:`C_n^2` profiles are collected from various sources, but mostly M. Schoek's surveys for the TMT sites, and M. Chun's survey at the Maunakea summit.

MD5 databases of the seeing profiles and an accumulated database of generated PSFs, is available.

|

.. image:: multiLGS.png

.. image:: nebula.jpg

--------

The following is just template example so I can put in math later if wanted:
Any math?

Any inline math :math:`e = mc^2`?

$$
e = mc^2
$$

:math:`e = mc^2`

.. math::

  e = h \nu


The area of a circle is :math:`A_\text{c} = (\pi/4) d^2`.

---------