#!/opt/local/bin/python
"""List or get contents of the PSF storage database
"""
import h5py
import tables
import pandas as pd
import json
import argparse
import pyfits
import textwrap

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=textwrap.dedent('''\
        Access the PSF storage database. This utility can be used to list (or optionally save to a file) information about
        the database, or to retrieve a PSF from it.
                             
        Absent the -i or -r option, create a summary table (directory) of what's available within the database and print it to the terminal.
        Note this summary table lists all the data entries (rows) but not all the properties (columns) for each.
        The list does provide the unique IDs for each entry, so then you can subsequently grab the PSF using the -r option
        or list full information about the PSF with the -i option.
        
        The full directory (with all rows and columns) can be sent to a file instead with the -o option.
        The output file is a pandas HDF5 file that can be read with python/pandas IO routines
        or, if the provided filename has the .csv extension, it will be in csv format, which can
        be read by spreadsheet programs like Excel, LibreOffice, etc.
                                 '''))
parser.add_argument('-r',type=str,help='retrieve the dataset by key',metavar='key')
parser.add_argument('-i',type=str,help='show info about a dataset',metavar='key')
default_dbfile = 'atest.h5'
parser.add_argument('-f',type=str,help='read from database (default is %s)'%default_dbfile,metavar='filename',default=default_dbfile)
parser.add_argument('-o',type=str,help='send table to output file in HDF5 or csv format',metavar='filename')
args = vars(parser.parse_args())

def prnode(name,obj):
    if isinstance(obj,h5py.Group):
        print 'Group: %s'%name
    elif isinstance(obj,h5py.Dataset):
        name = ''.join(x for x in name.split('/')[1:])
        if name == 'data':
            name += ' %s'%str(obj.shape)
        print '\tDataset: %s'%name

#f.visititems(prnode)
showcols = ['key','telescope','Profile','wavelength','DM_conjugate','field_angle_r','ENA_R']

def process(args):
    f = h5py.File(args['f'])
    #f = h5py.File('atest.h5')
    if args['i'] is not None:
        key = args['i']
        if key in f.keys():
            grp = f[key]
            if isinstance(grp,h5py.Group):
                attrs = pd.Series(json.loads(grp['attrs'].value))
                print attrs
                hdr = pd.Series(json.loads(grp['header'].value))
                print hdr
            else:
                print '%s does not have a header'%key
        else:
            print 'sorry, %s is not in the database'%key
    elif args['r'] is not None:
        key = args['r']
        if key in f.keys():
            grp = f[key]
            if isinstance(grp,h5py.Group):
                filename = key+'.fits'
                hdr = json.loads(grp['header'].value)
                attrs = json.loads(grp['attrs'].value)
                data = grp['data'].value
                hdu = pyfits.PrimaryHDU(data)
                n,m = data.shape
                # FITS standard keywords
                hdu.header['CRPIX1'] = n/2+1
                hdu.header['CRPIX2'] = m/2+1
                hdu.header['CDELT1'] = attrs['dx']
                hdu.header['CDELT2'] = attrs['dx']
                hdu.header['CRVAL1'] = 0.
                hdu.header['CRVAL2'] = 0.
                hdu.header['CTYPE1'] = attrs['dx_units']
                hdu.header['CTYPE2'] = attrs['dx_units']
                cnt = [len(hdu.header)]
                for d in [attrs,hdr]:
                    for dkey in d:
                        #print dkey,d[dkey]
                        fkey = dkey[:8].upper().replace(' ','_')
                        if isinstance(d[dkey],(float,int)):
                            hdu.header[fkey] = (d[dkey],dkey)
                        elif isinstance(d[dkey],(str,unicode)):
                            val = d[dkey].encode('ascii').replace('\n','<br>') #[:20]
                            hdu.header[fkey] = (val,dkey)
                    cnt.append(len(hdu.header))
                hdu.header.insert(cnt[0],('comment','--- start of InfoArray attributes ---'))
                hdu.header.insert(cnt[1],('comment','--- start of database cell values ---'))
                #print hdu.header.tostring(sep='\n')
                hdulist = pyfits.HDUList([hdu])
                hdulist.writeto(filename)
            else:
                print '%s does not have a header'%key
        else:
            print 'sorry, %s is not in the database'%key    
    else:
        s = []
        for key in f.keys():
            grp = f[key]
            if isinstance(grp,h5py.Group):
                if 'header' in grp.keys():
                    hdr = pd.Series(json.loads(grp['header'].value))
                    s.append(hdr)
        df = pd.DataFrame(s)
        pd.set_option('display.width',1000)
        bylist = ['telescope','Profile','DM_conjugate','wavelength','field_angle_r']
        if args['o'] is not None:   # output to a file
            filename = args['o']
            if isinstance(filename,str):
                if filename.endswith('.csv'):
                    df.to_csv(filename)
                else:
                    #f.close() # in case we want to write the DataFrame into the database file
                    store = pd.HDFStore(filename)
                    store['base'] = df
                    store.close()
        else:                       # output to the terminal
            pd.set_option('display.max_rows', None)
            print df[showcols].sort_values(by=bylist)
    
    try:
        f.close()
    except:
        pass

if __name__ == "__main__":
    process(args)
