#!/opt/local/bin/python
"""Print the directory of an HDF5 file
"""

import sys
import h5py
import argparse
import subprocess as sp

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description='Print the directory of an HDF file')
default_dbfile = 'atest.h5'
parser.add_argument('filename',type=str,help='HDF5 database file name',nargs='?')
parser.add_argument('-l','--level',type=int,help='depth',metavar='level')
parser.add_argument('-k','--key',type=str,help='list only elements under key',metavar='key')
args = vars(parser.parse_args())
key = args['key']
maxlevel = args['level']

if key is not None:
    keylevel = len(key.split('/'))
else:
    keylevel = 0

def prnode(name,obj):
    level = len(name.split('/')) - keylevel
    if key is not None:
        if not name.startswith(key):
            return
    if maxlevel is not None:
        if level > maxlevel:
            return
    print '%s'%name

if __name__ == '__main__':
    #print args
    if args['filename'] is None:
        parser.print_usage()
        print 'no filename'
        r = raw_input('list HDF files? [Y|n]: ')
        if r == 'Y':
            fl = sp.check_output(['ls']).split('\n')
            for f in fl:
                r = sp.check_output(['file',f])
                if 'Hierarchical Data Format' in r:
                    print '   %s'%f; sys.stdout.flush()
    else:
        f = h5py.File(args['filename'])
        f.visititems(prnode)
        f.close()


