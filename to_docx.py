#!/opt/local/bin/python
"""To use:
word_prep filename_base

This reads an html file that has been downloaded from a jupyter notebook,
strips the input code blocks and writes a MSWord docx file. The LaTeX equations
in the html file are converted the Word equation editor format.

"""
# ref: http://lxml.de/lxmlhtml.html
import sys, os
from lxml import html
import subprocess

basename = sys.argv[1]
basename = os.path.splitext(basename)[0] # remove the trailing .html if any
tree = html.parse('%s.html'%basename)
body = tree.getroot().find('body')
elements = body.find_class('input')
for element in elements:
    element.getparent().remove(element)

#html.open_in_browser(tree) ## debug
tempfile = '%s_temp.html'%basename
tree.write(tempfile,method='html')
bash_command = 'pandoc -f html+tex_math_dollars -t docx -o %s.docx %s'%(basename,tempfile)
output = subprocess.check_output(bash_command.split())
print output
output = subprocess.check_output(['rm',tempfile])
print output
